# Brakin power measurement app
App measure of breaking power level in cars reading from strain-gauge beam via web api.
Prototype was using components like: Raspberry Pi, strain-gauge beam, A/D converter, display(with 800x600 resolution).
Application has fixed position on every widget/element - I'm ashamed because of that.

## Getting started
### Requirements
  - Python3
  - PyQt5
  - requests

### Instalation
Ubuntu:
  - apt-get install python3
  - apt-get install python3-pip
  - apt-get install python3-pyqt5
  - pip3 install requests

If you haven't hardware, you can use mock server written in flask. You have to change `HOST` value in `core.py` to IP of flask server and add in 209 line in `gui2.py` parameter `filename`.

### Running
#### Linux:
```sh
$ python3 start.py
```
if using mock server:
```sh
$ export FLASK_APP=server.py
$ python3 -m flask run
$ python3 start.py
```
# License
MIT