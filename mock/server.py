from flask import Flask, jsonify
from random import randint
app = Flask(__name__)


par = {"green": 100, "yellow": 90}


@app.route("/parameters.php")
def hello():
    return jsonify(par)

@app.route("/value.php")
def sd():
    res = {"result": randint(0, 200)}
    return jsonify(res)

if __name__ == '__main__':
    app.run()