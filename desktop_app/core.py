import requests
from time import time, sleep


HOST = "http://localhost:80"
VALUE_URL = "value.php"
PARAMETERS_URL = "parameters.php"
PARAMETERS_PATH = "/var/www/html/parameters.php"


class Sensor():
    @staticmethod
    def measure():
        start_time = time()
        results = []
        for i in range(0,100):
            r = requests.get("{}/{}".format(HOST, VALUE_URL)).json()
            results.append(r['result'])
            sleep(0.05) # here you can change data download latency from sensor
        overall_time = time() - start_time
        return [max(results), overall_time]

    @staticmethod
    def change_peaks(green, yellow, filename=PARAMETERS_PATH):
        parameters = ("""<?php
$yellow = {yellow};
$green = {green};
$row = 1;
echo "{{" . '"yellow" : ' . {yellow} . "," . '"green" : ' . {green} . "}}";
?>""".format(yellow=yellow, green=green))
        with open(filename, "w") as f:
            f.write("{}".format(parameters))

    @staticmethod
    def read_peaks():
        r = requests.get("{}/{}".format(HOST,PARAMETERS_URL))
        return r.json()
