from PyQt5.QtGui import QFont

class Strings:
    TITLE = "Pomiar mocy hamowania"
    MAKE_MEASURE = "Wykonaj pomiar"
    SETTINGS = "Ustawienia"
    RESULT_TITLE = "Wynik pomiaru:"
    RESULT = "Wynik"
    MAKE_AGAIN = "Powtórz pomiar"
    CALIBRATION = "Kalibracja"
    CHANGE_UNIT = "Zmień jednostki"
    CALIB_CHOOSE = "Wybierz przedział"
    CALIB_GREEN = "Zielony powyżej: "
    CALIB_YELLOW = "Żółty powyżej: "
    CALIB_WARNING = "Tutaj wpisujemy tylko wartości liczbowe!"
    BACK = "< Powrót"
    CONFIRM = "Zatwierdź"
    GRADE_TITLE = "Ocena: "
    GRADE_WEAK = "Słabo"
    GRADE_GOOD = "Dobrze"
    GRADE_TOO_WEAK = "Za słabo"
    TIME_TITLE = "Czas pomiaru: "
    DATE_TITLE = "Data pomiaru: "


class TitleFont(QFont):
    def __init__(self, parent=None):
        super(TitleFont, self).__init__(parent)
        self.setPointSize(36)
        self.setFamily("Tahoma")

class SubTitleFont(QFont):
    def __init__(self, parent=None):
        super(SubTitleFont, self).__init__(parent)
        self.setPointSize(20)
        self.setFamily("Tahoma")

class CalibWarningFont(QFont):
    def __init__(self, parent=None):
        super(CalibWarningFont, self).__init__(parent)
        self.setItalic(1)
        self.setBold(1)
        self.setPointSize(20)
        self.setFamily("Tahoma")

class ResultPropertiesFont(QFont):
    def __init__(self, parent=None):
        super(ResultPropertiesFont, self).__init__(parent)
        self.setPointSize(16)
        self.setFamily("Tahoma")