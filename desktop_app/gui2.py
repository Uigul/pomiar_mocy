from PyQt5.QtWidgets import *
from PyQt5.QtGui import QPainter, QColor, QFont
from styles import *
from core import Sensor
from time import strftime

class ChartBar(QWidget):

    def __init__(self, value, peaks, parent=None):
        super(ChartBar, self).__init__(parent)
        self.peaks = peaks
        self.value = value
        self.initUI()

    def initUI(self):
        self.description = [200, 160, 120, 80, 40, 0]
        self.setGeometry(0, 0, 200, 400)
        self.show()

    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)
        self.drawText(event, qp)
        self.drawBar(event, qp)
        qp.end()

    def drawBar(self, event, qp):
        if self.peaks == 'green':
            qp.setBrush(QColor(0, 255, 0))
        if self.peaks == 'yellow':
            qp.setBrush(QColor(255, 255, 0))
        if self.peaks == 'red':
            qp.setBrush(QColor(255, 0, 0))
        qp.setPen(QColor(0, 0, 0))
        qp.drawRect(45, 399, 140, -self.value*2)

    def drawText(self, event, qp):
        qp.setBrush(QColor(0, 0, 0))
        qp.setPen(QColor(25, 25, 25))
        qp.setFont(QFont('Tahoma', 10))
        for i, j in zip(self.description, reversed(self.description)):
            qp.drawRect(25, 79+j*2, 180, 0)
            qp.drawText(0, j*2, str(i))


class WelcomeTab(QWidget):
    def __init__(self, parent=None):
        super(WelcomeTab, self).__init__(parent)
        self._init()

    def _init(self):
        # declare components
        self.start_button = QPushButton(Strings.MAKE_MEASURE, self)
        self.settings_button = QPushButton(Strings.SETTINGS, self)
        title = QLabel(Strings.TITLE, self)

        # modify components
        title_font = TitleFont()
        title.setFont(title_font)
        self.start_button.setFixedSize(200, 200)
        self.settings_button.setFixedSize(200, 200)

        # placement of components
        title.move(100, 80)
        self.start_button.move(190, 260)
        self.settings_button.move(410, 260)


class ResultTab(QWidget):
    def __init__(self, parent=None):
        super(ResultTab, self).__init__(parent)
        self._init()

    def _init(self):
        result = Sensor.measure()
        peaks = Sensor.read_peaks()

        # declare components
        self.back_button = QPushButton(Strings.BACK, self)
        result_title = QLabel(Strings.RESULT_TITLE, self)
        self.result_label = QLabel("{} N".format(str(round(result[0], 2))), self)
        time_title_label = QLabel(Strings.TIME_TITLE, self)
        time_label = QLabel("{} s".format(round(result[1], 2)), self)
        date_title_label = QLabel(Strings.DATE_TITLE, self)
        date_label= QLabel(strftime("%d.%m.%Y"), self)
        grade_title_label = QLabel(Strings.GRADE_TITLE, self)
        grade_label = QLabel("", self)

        # modify components
        self.back_button.setFixedSize(100, 40)
        result_title_font = TitleFont()
        result_title.setFont(result_title_font)
        font = QFont()
        font.setPointSize(50)
        self.result_label.setFont(font)
        properties_font = ResultPropertiesFont()
        time_title_label.setFont(properties_font)
        time_label.setFont(properties_font)
        date_title_label.setFont(properties_font)
        date_label.setFont(properties_font)
        grade_title_label.setFont(properties_font)
        grade_label.setFont(properties_font)
        if result[0] < peaks['yellow']:
            grade_label.setText(Strings.GRADE_TOO_WEAK)
            grade_label.setStyleSheet('color: red')
            chart_bar = ChartBar(result[0], 'red', self)
        elif result[0] > peaks['green']:
            grade_label.setText(Strings.GRADE_GOOD)
            grade_label.setStyleSheet('color: green')
            chart_bar = ChartBar(result[0], 'green', self)
        else:
            grade_label.setText(Strings.GRADE_WEAK)
            grade_label.setStyleSheet('color: orange')
            chart_bar = ChartBar(result[0], 'yellow', self)

        # placement of components
        self.back_button.move(10, 10)
        result_title.move(50,80)
        self.result_label.move(120,200)
        time_title_label.move(10, 340)
        time_label.move(200, 340)
        date_title_label.move(10, 380)
        date_label.move(200, 380)
        grade_title_label.move(10, 420)
        grade_label.move(200, 420)

        chart_bar.move(540, 100)



class Calibration(QWidget):
    def __init__(self, parent=None):
        super(Calibration, self).__init__(parent)
        self._init()

    def _init(self):
        peaks = Sensor.read_peaks()

        # declare components
        self.back_button = QPushButton(Strings.BACK, self)
        title = QLabel(Strings.CALIBRATION, self)
        sub_title = QLabel(Strings.CALIB_CHOOSE, self)
        yellow_label = QLabel(Strings.CALIB_YELLOW, self)
        green_label = QLabel(Strings.CALIB_GREEN, self)
        self.yellow = QLineEdit(str(peaks['yellow']), self)
        self.green = QLineEdit(str(peaks['green']), self)
        self.confirm_button = QPushButton(Strings.CONFIRM, self)
        self.warning = QLabel(Strings.CALIB_WARNING, self)

        # modify components
        self.back_button.setFixedSize(100, 40)
        title_font = TitleFont()
        title.setFont(title_font)
        sub_title_font = SubTitleFont()
        sub_title.setFont(sub_title_font)
        self.confirm_button.setFixedSize(320, 40)
        warning_font = CalibWarningFont()
        self.warning.setFont(warning_font)
        self.warning.setStyleSheet('color: red')

        # placement of components
        self.back_button.move(10, 10)
        title.move(50, 80)
        sub_title.move(80, 160)
        yellow_label.move(200, 220)
        green_label.move(200, 260)
        self.yellow.move(360, 220)
        self.green.move(360, 260)
        self.confirm_button.move(200, 400)
        self.warning.move(50, 500)
        self.warning.hide()


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setFixedSize(800, 600)
        self.brakin_power_welcome()

    def brakin_power_welcome(self):
        self.window = WelcomeTab(self)
        self.window.start_button.setEnabled(1)
        self.setWindowTitle("Pomiar mocy hamowania")
        self.setCentralWidget(self.window)
        self.window.settings_button.clicked.connect(self.settings)
        self.window.start_button.clicked.connect(self.results)
        self.show()

    def results(self):
        self.window = ResultTab(self)
        self.setWindowTitle("Pomiar mocy hamowania - WYNIKI")
        self.setCentralWidget(self.window)
        self.window.back_button.clicked.connect(self.brakin_power_welcome)
        self.show()

    def settings(self):
        self.window.start_button.setEnabled(0)
        self.window = Calibration(self)
        self.setWindowTitle("Pomiar mocy hamowania - USTAWIENIA")
        self.setCentralWidget(self.window)
        self.window.back_button.clicked.connect(self.brakin_power_welcome)
        self.window.confirm_button.clicked.connect(self.confirm_calib_signal)
        self.show()

    def confirm_calib_signal(self):
        try:
            yellow = int(self.window.yellow.text())
            green = int(self.window.green.text())
            Sensor.change_peaks(yellow=yellow, green=green)
            self.brakin_power_welcome()
        except ValueError:
            self.window.warning.show()
